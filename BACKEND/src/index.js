/**
 * Autor: Juan Diego Romero
 * fecha: 2/10/2021
 * Descripción: Se importa Express, Mongodb, db
 *              Se conecta con carpeta Routers
 *              Se crea objeto para conectar a la base de datos
 * Clase: index.js
 */

//Importar express
const express = require('express');
const territoryRouter = require('./routers/territoryRouter');
const conectarBD = require("./database/conectarBD");

const cors =require('cors');

class Server{
    constructor(){
        const objconectarBD=new conectarBD();
        this.app = express();
        //Se llama el metodo config para levantar el servidor
        this.config();
        
    }
    config(){
        //Indicar el puerto por el que se ejecutará el servidor
        this.app.set('port', process.env.PORT || 3000);
        //Indicar que las solicitudes http se trabajará en JSON
        this.app.use(express.json());
        this.app.use(cors());
        /**
         * ******************Rutas**********************
         * ******/
        const router = express.Router();
        router.get('/', (req, res)=>{
            res.status(200).json({message: "Nueva conexión"});
        });
        const territoryR = new territoryRouter();
        
        //añadir las rutas al servidor
        this.app.use(territoryR.router);
        this.app.use(router);
        //Levantar el servidor/correr el servidor
        this.app.listen(this.app.get('port'), ()=>{
            console.log("Servidor corriendo por el puerto => ", this.app.get('port'));
        });

    }

}

const objServer = new Server();



